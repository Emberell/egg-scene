//  import {spawnGltfX} from 'modules/SpawnerFunctions'

//    let EggShape = new GLTFShape('models/Egg.glb')
//    let tree1 = spawnGltfX(tree1Shape, 6,0,6,	0,0,0,		1,1,1)

import {BuilderHUD} from 'modules/BuilderHUD'

const scene = new Entity()
const scenetransform = new Transform({ position: new Vector3(0, 0, 0), rotation: Quaternion.Euler(0, 0, 0), scale: new Vector3(1, 1, 1) })
scene.addComponent(scenetransform)
engine.addEntity(scene)

const Egg = new Entity()

Egg.addComponent(new Transform())

Egg.getComponent(Transform).position.set(3, 1, 3)

Egg.getComponent(Transform).scale.set(1, 1, 1)

const green = new Material()
green.albedoColor = Color3.Green()

Egg.addComponent(green)

Egg.addComponent(new GLTFShape("models/Egg.glb"))

engine.addEntity(Egg)

const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(Egg)
hud.setDefaultParent(scene)

export function spawnEntity(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    // create the entity
    const entity = new Entity()
    // set a transform to the entity
    const transform = new Transform({ position: new Vector3(x, y, z) })
    transform.rotation.setEuler(rx, ry, rz)
    transform.scale.set(sx, sy, sz)
    entity.addComponent(transform)
    // add the entity to the engine
    engine.addEntity(entity)
    return entity
  }
  
  export function spawnBoxX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(new BoxShape())
    return entity
  }
  
  export function spawnBox(x: number, y: number, z: number) {
    return spawnBoxX(x,y,z,0,0,0,1,1,1)
  }
  
  export function spawnConeX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(new ConeShape())
    return entity
  }
  
  export function spawnCone(x: number, y: number, z: number) {
    return spawnConeX(x,y,z,0,0,0,1,1,1)
  }
  
  export function spawnCylinderX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(new CylinderShape())
    return entity
  }
  
  export function spawnCylinder(x: number, y: number, z: number) {
    return spawnCylinderX(x,y,z,0,0,0,1,1,1)
  }
  
  export function spawnGltfX(s: GLTFShape, x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(s)
    return entity
  }
  
  export function spawnGltf(s: GLTFShape, x: number, y: number, z: number) {
    return spawnGltfX(s,x,y,z,0,0,0,1,1,1)
  }
  
  export function spawnPlaneX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(new PlaneShape())
    return entity
  }
  
  export function spawnPlane(x: number, y: number, z: number) {
    return spawnPlaneX(x,y,z,0,0,0,1,1,1)
  }
  
  export function spawnSphereX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(new SphereShape())
    return entity
  }
  
  export function spawnSphere(x: number, y: number, z: number) {
    return spawnSphereX(x,y,z,0,0,0,1,1,1)
  }
  
  export function spawnTextX(value: string, x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number) {
    const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz)
    // set a shape to the entity
    entity.addComponent(new TextShape(value))
    return entity
  }
  
  export function spawnText(value: string, x: number, y: number, z: number) {
    return spawnTextX(value,x, y,z,0,0,0,1,1,1)
  }
  
  